﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptPuzzle : MonoBehaviour
{
    // Start is called before the first frame update
    public Text input;
    public GameObject closeChest;
    public GameObject openChest;
    public ParticleSystem chest;

    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
        if(input.text == "Cañón.")
        {
            closeChest.SetActive(false);
            openChest.SetActive(true);
            if (!chest.isPlaying)
            {
                chest.Play();
            }
        }
    }
}
